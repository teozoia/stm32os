#ifndef _GPIO_
#define _GPIO_

#include <inttypes.h>
#include <cpu.h>

typedef struct {
    __io uint32_t MODER;    /*!< GPIO port mode register,               Address offset: 0x00      */
    __io uint32_t OTYPER;   /*!< GPIO port output type register,        Address offset: 0x04      */
    __io uint32_t OSPEEDR;  /*!< GPIO port output speed register,       Address offset: 0x08      */
    __io uint32_t PUPDR;    /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
    __io uint32_t IDR;      /*!< GPIO port input data register,         Address offset: 0x10      */
    __io uint32_t ODR;      /*!< GPIO port output data register,        Address offset: 0x14      */
    __io uint32_t BSRR;     /*!< GPIO port bit set/reset register,      Address offset: 0x18      */
    __io uint32_t LCKR;     /*!< GPIO port configuration lock register, Address offset: 0x1C      */
    __io uint32_t AFR[2];   /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
} GPIO_t;

typedef struct {
	uint8_t GPIO_PinNumber;
	uint8_t GPIO_PinMode;
	uint8_t GPIO_PinSpeed;
	uint8_t GPIO_PinPUPDControl;
	uint8_t GPIO_PinOPType;
	uint8_t GPIO_PinAltFunMode;
} GPIO_PinConfig_t;

typedef struct {
	// pointer to hold the base address of the gpio peripheral
	GPIO_t *pGPIOx; // this holds the base address of the gpio port to which the pin belongs
	GPIO_PinConfig_t GPIO_PinConfig; // this holds the pin configuration settings
} GPIO_Handle_t;


#define GPIOA_BASEADDR	(AHB1PERIPH_BASE + 0x0000U)
#define GPIOB_BASEADDR	(AHB1PERIPH_BASE + 0x0400U)
#define GPIOC_BASEADDR	(AHB1PERIPH_BASE + 0x0800U)
#define GPIOD_BASEADDR	(AHB1PERIPH_BASE + 0x0C00U)
#define GPIOE_BASEADDR	(AHB1PERIPH_BASE + 0x1000U)
#define GPIOF_BASEADDR	(AHB1PERIPH_BASE + 0x1400U)
#define GPIOG_BASEADDR	(AHB1PERIPH_BASE + 0x1800U)
#define GPIOH_BASEADDR	(AHB1PERIPH_BASE + 0x1C00U)
#define GPIOI_BASEADDR	(AHB1PERIPH_BASE + 0x2000U)
#define GPIOJ_BASEADDR	(AHB1PERIPH_BASE + 0x2400U)
#define GPIOK_BASEADDR	(AHB1PERIPH_BASE + 0x2800U)

#define GPIOA	((GPIO_t *)GPIOA_BASEADDR)
#define GPIOB	((GPIO_t *)GPIOB_BASEADDR)
#define GPIOC	((GPIO_t *)GPIOC_BASEADDR)
#define GPIOD	((GPIO_t *)GPIOD_BASEADDR)
#define GPIOE	((GPIO_t *)GPIOE_BASEADDR)
#define GPIOF	((GPIO_t *)GPIOF_BASEADDR)
#define GPIOG	((GPIO_t *)GPIOG_BASEADDR)
#define GPIOH	((GPIO_t *)GPIOH_BASEADDR)
#define GPIOI	((GPIO_t *)GPIOI_BASEADDR)
#define GPIOJ	((GPIO_t *)GPIOJ_BASEADDR)
#define GPIOK	((GPIO_t *)GPIOK_BASEADDR)

#define GPIOA_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 0) )
#define GPIOB_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 1) )
#define GPIOC_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 2) )
#define GPIOD_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 3) )
#define GPIOE_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 4) )
#define GPIOF_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 5) )
#define GPIOG_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 6) )
#define GPIOH_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 7) )
#define GPIOI_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 8) )
#define GPIOJ_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 9) )
#define GPIOK_CLOCK_ENABLE()	(RCC->AHB1ENR  |= (1U << 10) )

#define GPIOA_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 0) )
#define GPIOB_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 1) )
#define GPIOC_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 2) )
#define GPIOD_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 3) )
#define GPIOE_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 4) )
#define GPIOF_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 5) )
#define GPIOG_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 6) )
#define GPIOH_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 7) )
#define GPIOI_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 8) )
#define GPIOJ_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 9) )
#define GPIOK_CLOCK_DISABLE()	(RCC->AHB1ENR  &= ~(1U << 10) )

#define GPIOA_REG_RESET()			do {(RCC->AHB1RSTR |=(1 <<  0)); (RCC->AHB1RSTR &= ~(1U <<  0));}while(0)
#define GPIOB_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  1)); (RCC->AHB1RSTR &= ~(1U <<  1));}while(0)
#define GPIOC_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  2)); (RCC->AHB1RSTR &= ~(1U <<  2));}while(0)
#define GPIOD_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  3)); (RCC->AHB1RSTR &= ~(1U <<  3));}while(0)
#define GPIOE_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  4)); (RCC->AHB1RSTR &= ~(1U <<  4));}while(0)
#define GPIOF_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  5)); (RCC->AHB1RSTR &= ~(1U <<  5));}while(0)
#define GPIOG_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  6)); (RCC->AHB1RSTR &= ~(1U <<  6));}while(0)
#define GPIOH_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  7)); (RCC->AHB1RSTR &= ~(1U <<  7));}while(0)
#define GPIOI_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  8)); (RCC->AHB1RSTR &= ~(1U <<  8));}while(0)
#define GPIOJ_REG_RESET()           do {(RCC->AHB1RSTR |=(1 <<  9)); (RCC->AHB1RSTR &= ~(1U <<  9));}while(0)
#define GPIOK_REG_RESET()           do {(RCC->AHB1RSTR |=(1 << 10)); (RCC->AHB1RSTR &= ~(1U << 10));}while(0)

// clock control api for gpio
void GPIO_PeripClockControl(GPIO_t *pGPIOx, uint8_t EnorDi);

// gpio init and deinit apis
void GPIO_Init(GPIO_Handle_t* pGPIOHandle);
void GPIO_DeInit(GPIO_t *pGPIOx);

// gpio read and write to port or pin
uint8_t GPIO_ReadFromInputPin(GPIO_t *pGPIOx, uint8_t PinNumber);
uint16_t GPIO_ReadFromInputPort(GPIO_t *pGPIOx);
void GPIO_WriteToOutputPin(GPIO_t *pGPIOx, uint8_t PinNumber, uint8_t Value);
void GPIO_WriteToOutputPort(GPIO_t *pGPIOx, uint16_t Value);
void GPIO_ToggleOutputPin(GPIO_t *pGPIOx, uint8_t PinNumber);

// // gpio IRQ config and handling
// void GPIO_IRQConfig(uint8_t IRQ_Number, uint8_t EnorDi);
// void GPIO_IRQHandling(uint8_t PinNumber);
// void GPIO_IRQ_ProrityConfig(uint8_t IRQ_Number, uint32_t Interrupt_Prority);


#define GPIO_PIN_SET 	1
#define GPIO_PIN_RESET 	0

//GPIO pin numbers
#define GPIO_0		0
#define GPIO_1		1
#define GPIO_2		2
#define GPIO_3		3
#define GPIO_4		4
#define GPIO_5		5
#define GPIO_6		6
#define GPIO_7		7
#define GPIO_8		8
#define GPIO_9		9
#define GPIO_10		10
#define GPIO_11		11
#define GPIO_12		12
#define GPIO_13		13
#define GPIO_14		14
#define GPIO_15		15

//  GPIO pin modes
#define GPIO_MODE_INPUT 	0
#define GPIO_MODE_OUTPUT	1
#define GPIO_MODE_ALTFN		2
#define GPIO_MODE_ANALOGE	3
#define GPIO_MODE_IT_FT		4
#define GPIO_MODE_IT_RT		5
#define GPIO_MODE_IT_RFT	6

// GPIO output types
#define GPIO_OP_TYPE_PP		0
#define GPIO_OP_TYPE_OD		1

// GPIO pin Speeds
#define GPIO_SPEED_LOW			0
#define GPIO_SPEED_MEDIUM		1
#define GPIO_SPEED_FAST			2
#define GPIO_SPEED_HIGH			3

//GPIO pin pull up pull down
#define GPIO_NO_PUPD			0
#define GPIO_PIN_PD				2
#define GPIO_PIN_PU				1

#endif