#include <inttypes.h>
#include <stdbool.h>
#include <cpu.h>
#include <gpio.h>

#define LEDDELAY    1000000

int main(void);
void delay(volatile uint32_t);

int main(void)
{
    GPIO_Handle_t GpioLed ;
	GpioLed.pGPIOx = GPIOD ;
	GPIOD_CLOCK_ENABLE() ;

	GpioLed.GPIO_PinConfig.GPIO_PinNumber = GPIO_14 ;
	GpioLed.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUTPUT ;
	GpioLed.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP ;
	GpioLed.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH ;
	GpioLed.GPIO_PinConfig.GPIO_PinPUPDControl = GPIO_NO_PUPD ;
	GpioLed.GPIO_PinConfig.GPIO_PinAltFunMode = 0 ;
	GPIO_Init(&GpioLed);

	GpioLed.GPIO_PinConfig.GPIO_PinNumber = GPIO_13 ;
	GpioLed.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUTPUT ;
	GpioLed.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP ;
	GpioLed.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH ;
	GpioLed.GPIO_PinConfig.GPIO_PinPUPDControl = GPIO_NO_PUPD ;
	GpioLed.GPIO_PinConfig.GPIO_PinAltFunMode = 0 ;
	GPIO_Init(&GpioLed) ;

    GPIO_WriteToOutputPin(GPIOD, GPIO_14, GPIO_PIN_SET);

	while(1){
		GPIO_ToggleOutputPin(GPIOD, GPIO_13);
		delay(LEDDELAY);
	}

    return 0;
}

void delay(volatile uint32_t s)
{
    for(; s>0; s--);
}

// Startup code
__attribute__((naked, noreturn)) void _reset(void) {
  // memset .bss to zero, and copy .data section to RAM region
  extern long _sbss, _ebss, _sdata, _edata, _sidata;
  for (long *src = &_sbss; src < &_ebss; src++) *src = 0;
  for (long *src = &_sdata, *dst = &_sidata; src < &_edata;) *src++ = *dst++;

  main();             // Call main()
  for (;;) (void) 0;  // Infinite loop in the case if main() returns
}

extern void _estack(void);  // Defined in link.ld

__attribute__((section(".isr_vector"))) void (*tab[16 + 91])(void) = {_estack, _reset};