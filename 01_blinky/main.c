#include <inttypes.h>
#include <stdbool.h>

typedef struct
{
    uint32_t CR;            /*!< RCC clock control register,                                  Address offset: 0x00 */
    uint32_t PLLCFGR;       /*!< RCC PLL configuration register,                              Address offset: 0x04 */
    uint32_t CFGR;          /*!< RCC clock configuration register,                            Address offset: 0x08 */
    uint32_t CIR;           /*!< RCC clock interrupt register,                                Address offset: 0x0C */
    uint32_t AHB1RSTR;      /*!< RCC AHB1 peripheral reset register,                          Address offset: 0x10 */
    uint32_t AHB2RSTR;      /*!< RCC AHB2 peripheral reset register,                          Address offset: 0x14 */
    uint32_t AHB3RSTR;      /*!< RCC AHB3 peripheral reset register,                          Address offset: 0x18 */
    uint32_t RESERVED0;     /*!< Reserved, 0x1C                                                                    */
    uint32_t APB1RSTR;      /*!< RCC APB1 peripheral reset register,                          Address offset: 0x20 */
    uint32_t APB2RSTR;      /*!< RCC APB2 peripheral reset register,                          Address offset: 0x24 */
    uint32_t RESERVED1[2];  /*!< Reserved, 0x28-0x2C                                                               */
    uint32_t AHB1ENR;       /*!< RCC AHB1 peripheral clock register,                          Address offset: 0x30 */
    uint32_t AHB2ENR;       /*!< RCC AHB2 peripheral clock register,                          Address offset: 0x34 */
    uint32_t AHB3ENR;       /*!< RCC AHB3 peripheral clock register,                          Address offset: 0x38 */
    uint32_t RESERVED2;     /*!< Reserved, 0x3C                                                                    */
    uint32_t APB1ENR;       /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x40 */
    uint32_t APB2ENR;       /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x44 */
    uint32_t RESERVED3[2];  /*!< Reserved, 0x48-0x4C                                                               */
    uint32_t AHB1LPENR;     /*!< RCC AHB1 peripheral clock enable in low power mode register, Address offset: 0x50 */
    uint32_t AHB2LPENR;     /*!< RCC AHB2 peripheral clock enable in low power mode register, Address offset: 0x54 */
    uint32_t AHB3LPENR;     /*!< RCC AHB3 peripheral clock enable in low power mode register, Address offset: 0x58 */
    uint32_t RESERVED4;     /*!< Reserved, 0x5C                                                                    */
    uint32_t APB1LPENR;     /*!< RCC APB1 peripheral clock enable in low power mode register, Address offset: 0x60 */
    uint32_t APB2LPENR;     /*!< RCC APB2 peripheral clock enable in low power mode register, Address offset: 0x64 */
    uint32_t RESERVED5[2];  /*!< Reserved, 0x68-0x6C                                                               */
    uint32_t BDCR;          /*!< RCC Backup domain control register,                          Address offset: 0x70 */
    uint32_t CSR;           /*!< RCC clock control & status register,                         Address offset: 0x74 */
    uint32_t RESERVED6[2];  /*!< Reserved, 0x78-0x7C                                                               */
    uint32_t SSCGR;         /*!< RCC spread spectrum clock generation register,               Address offset: 0x80 */
    uint32_t PLLI2SCFGR;    /*!< RCC PLLI2S configuration register,                           Address offset: 0x84 */
} RCC_TypeDef;

typedef struct
{
    uint32_t MODER;    /*!< GPIO port mode register,               Address offset: 0x00      */
    uint32_t OTYPER;   /*!< GPIO port output type register,        Address offset: 0x04      */
    uint32_t OSPEEDR;  /*!< GPIO port output speed register,       Address offset: 0x08      */
    uint32_t PUPDR;    /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
    uint32_t IDR;      /*!< GPIO port input data register,         Address offset: 0x10      */
    uint32_t ODR;      /*!< GPIO port output data register,        Address offset: 0x14      */
    uint32_t BSRR;     /*!< GPIO port bit set/reset register,      Address offset: 0x18      */
    uint32_t LCKR;     /*!< GPIO port configuration lock register, Address offset: 0x1C      */
    uint32_t AFR[2];   /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
} GPIO_TypeDef;

#define LEDDELAY    1000000
#define PERIPH_BASE           0x40000000U //!< Peripheral base address in the alias region    
#define AHB1PERIPH_BASE       (PERIPH_BASE + 0x00020000U)
#define RCC_BASE              (AHB1PERIPH_BASE + 0x3800U)
#define RCC                 ((RCC_TypeDef *) RCC_BASE)

#define GPIOD_BASE            (AHB1PERIPH_BASE + 0x0C00U)
#define GPIOD               ((GPIO_TypeDef *) GPIOD_BASE)

int main(void);
void delay(volatile uint32_t);

int main(void)
{
    /* Each module is powered separately. In order to turn on a module
     * its relevant clock needs to be enabled first.
     * The clock enables are located at RCC module in the relevant bus registers
     * STM32F4-Discovery board LEDs are connected to GPIOD pins 12, 13, 14, 15.
     * All the GPIO are connected to AHB1 bus, so the relevant register for the
     * clocks is AHB1ENR.
     */

    /* Enable GPIOD clock (AHB1ENR: bit 3) */
    // AHB1ENR: XXXX XXXX XXXX XXXX XXXX XXXX XXXX 1XXX
    RCC->AHB1ENR |= 0x00000008; // or RCC->AHB1ENR |= (1U << 3);

    /* Make Pin 12 output (MODER: bits 25:24) */
    // Each pin is represented with two bits on the MODER register
    // 00 - input (reset state)
    // 01 - output
    // 10 - alternate function
    // 11 - analog mode

    // We can leave the pin at default value to make it an input pin.
    // In order to make a pin output, we need to write 01 to the relevant
    // section in MODER register
    // We first need to AND it to reset them, then OR it to set them.
    //                     bit31                                         bit0
    // MODER register bits : xx xx xx 01 XX XX XX XX XX XX XX XX XX XX XX XX
    //                      p15      p12                                  p0
    GPIOD->MODER &= 0xFCFFFFFF;   // Reset bits 25:24 to clear old values
    GPIOD->MODER |= 0x01000000;   // Set MODER bits 25:24 to 01

    /* We do not need to change the speed of the pins, leave them floating
     * and leave them as push-pull, so no need to touch OTYPER, OSPEEDR, and OPUPDR
     * However, if we want to use a open-drain required protocol (i.e. I2C), we need
     * to declare the pin as open-drain from OTYPER register
     */
    /* Set or clear pins (ODR: bit 12) */
    // Set pin 12 to 1 to turn on an LED
    // ODR: xxx1 XXXX XXXX XXXX
    GPIOD->ODR |= 0x1000;

    while(1)
    {
        delay(LEDDELAY);
        GPIOD->ODR ^= (1U << 12);  // Toggle LED
    }

    __asm("NOP"); // Assembly inline can be used if needed
    return 0;
}

void delay(volatile uint32_t s)
{
    for(; s>0; s--);
}

// Startup code
__attribute__((naked, noreturn)) void _reset(void) {
  // memset .bss to zero, and copy .data section to RAM region
  extern long _sbss, _ebss, _sdata, _edata, _sidata;
  for (long *src = &_sbss; src < &_ebss; src++) *src = 0;
  for (long *src = &_sdata, *dst = &_sidata; src < &_edata;) *src++ = *dst++;

  main();             // Call main()
  for (;;) (void) 0;  // Infinite loop in the case if main() returns
}

extern void _estack(void);  // Defined in link.ld

__attribute__((section(".isr_vector"))) void (*tab[16 + 91])(void) = {_estack, _reset};